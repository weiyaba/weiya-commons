package com.weiya.commons.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/**
 * LocalNumberUtils
 *
 * @author 奕超
 * @date 2017/10/31
 */
public class LocalNumberUtils {

    public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    public static Long convertStr2Long(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        try {
            return Long.parseLong(str);
        } catch (Exception e) {

        }
        return null;
    }

    public static List<Long> convertStr2LongList(String str) {
        return convertStr2LongList(str, ",");
    }

    public static List<Long> convertStr2LongList(String str, String separator) {
        if (StringUtils.isBlank(str) || Objects.isNull(separator)) {
            return null;
        }
        List<Long> result = new ArrayList<>();
        String[] strArray = StringUtils.split(str, separator);
        for (String longStr : strArray) {
            Long element = convertStr2Long(longStr);
            if (Objects.nonNull(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static boolean isLongNullOrZero(Long l) {
        return Objects.isNull(l) || l.compareTo(0L) == 0;
    }

    public static Integer convertStr2Integer(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {

        }
        return null;
    }

    public static boolean isPositive(Long l) {
        return Objects.nonNull(l) && l > 0;
    }

    public static Long halfUpMultiplyByPercent(Long org, Integer percent) {
        if (Objects.isNull(org) || Objects.isNull(percent)) {
            return null;
        }
        BigDecimal orgDecimal = new BigDecimal(org);
        BigDecimal percentDecimal = new BigDecimal(percent).divide(ONE_HUNDRED);
        return orgDecimal.multiply(percentDecimal).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
    }

    public static Long halfDownDivideByPercent(Long org, Integer percent) {
        if (Objects.isNull(org) || Objects.isNull(percent)) {
            return null;
        }
        BigDecimal orgDecimal = new BigDecimal(org);
        BigDecimal percentDecimal = new BigDecimal(percent).divide(ONE_HUNDRED);
        return orgDecimal.divide(percentDecimal, 0, BigDecimal.ROUND_HALF_DOWN).longValue();
    }

    public static Long getOppositeNumber(Long number) {
        if (Objects.isNull(number)) {
            return number;
        }
        return 0 - number;
    }
}