package com.weiya.commons.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类 <br/>
 *
 * @author dufu
 * @date 2018/7/4 上午10:19
 * @Copyright 北京伊电园网络科技有限公司 2016-2017 © 版权所有 京ICP备17000101号
 */
public class DateUtils extends DateUtil {

    /**
     * 获取某天的结束时间(不包括毫秒值）
     */
    public static DateTime endOfDayWithoutMillisecond(Date date) {
        return new DateTime(endOfDayWithoutMillisecond(calendar(date)));
    }

    /**
     * 获取某天的结束时间(不包括毫秒值）
     */
    public static Calendar endOfDayWithoutMillisecond(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * LocalDateTime To Date
     */
    public static Date toDate(LocalDateTime localDateTime) {
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    /**
     * Date To LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }
}
