package com.weiya.commons.util;

import cn.hutool.core.collection.CollectionUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;

/**
 * LocalListUtils
 *
 * @author 奕超
 * @date 2017/3/28
 */
public class LocalListUtils {

    public static List<Long> stringListToLongList(List<String> stringList) {
        if (CollectionUtil.isEmpty(stringList)) {
            return null;
        }
        List<Long> result = new ArrayList<>();
        stringList.forEach(s -> result.add(LocalNumberUtils.convertStr2Long(s)));
        return result;
    }

    public static List<String> spiltStringToList(String str, String separator) {
        if (StringUtils.isBlank(str) || StringUtils.isBlank(separator)) {
            return null;
        }
        String[] arr = StringUtils.split(str, separator);
        return Stream.of(arr).collect(Collectors.toList());
    }

    public static <T, V> List<V> transferList(List<T> orgList, Function<T, V> function) {
        if (CollectionUtil.isEmpty(orgList)) {
            return null;
        }
        if (Objects.isNull(function)) {
            return null;
        }
        List<V> result = new ArrayList<>();
        orgList.forEach(t -> result.add(function.apply(t)));
        return result;
    }
}
