package com.weiya.commons.util;

import java.util.Objects;

/**
 * LocalObjectUtils
 *
 * @author 奕超
 * @date 2018/5/18
 */
public class LocalObjectUtils {

    public static boolean anyNull(Object... objectList) {
        if (Objects.isNull(objectList)) {
            return true;
        }
        for (Object obj : objectList) {
            if (Objects.isNull(obj)) {
                return true;
            }
        }
        return false;
    }

    public static boolean allNull(Object... objectList) {
        if (Objects.isNull(objectList)) {
            return true;
        }
        for (Object obj : objectList) {
            if (Objects.nonNull(obj)) {
                return false;
            }
        }
        return true;
    }

}
