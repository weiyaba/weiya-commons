package com.weiya.commons.codec;

import org.apache.commons.codec.binary.Base64;

/**
 * Base64 <br/>
 *
 * @author dufu
 * @date 2018/6/7 下午3:53
 * @Copyright 北京伊电园网络科技有限公司 2016-2017 © 版权所有 京ICP备17000101号
 */
public class Base64Utils {

    /**
     * 加密
     */
    public static String encrypt(String plainText) {
        byte[] b = plainText.getBytes();
        return Base64.encodeBase64URLSafeString(b);
    }

    /**
     * 解密
     */
    public static String decrypt(String cipherText) {
        byte[] b = cipherText.getBytes();
        Base64 base64 = new Base64();
        b = base64.decode(b);
        return new String(b);
    }

}