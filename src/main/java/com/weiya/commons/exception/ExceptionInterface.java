package com.weiya.commons.exception;


public interface ExceptionInterface {

    Integer getCode();

    String getMsg();
}
