package com.weiya.commons.exception;

import com.weiya.commons.enums.EnumInterface;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created with eden
 *
 * @Author: bailong Date: 2018/1/3 Time: 下午3:33 北京伊电园网络科技有限公司 2016-2017 © 版权所有 京ICP备17000101号
 */
@Getter
@AllArgsConstructor
public enum ErrorCodeEnum implements EnumInterface<ErrorCodeEnum> {

    //Success Codes
    OK(200, "成功的请求"),
    CREATED(201, "请求已经被实现"),
    ACCEPTED(202, "服务器已接受请求，但尚未处理"),
    NON_AUTHORITATIVE_INFORMATION(203, "非权威信息"),
    NO_CONTENT(204, "无内容"),
    RESET_CONTENT(205, "重置内容"),
    PARTIAL_CONTENT(206, "部分内容"),

    //REDIRECTION CODES
    MULTIPLE_CHOICES(300, "多种选择"),
    MOVED_PERMANENTLY(301, "永久重定向"),
    MOVED_TEMPORARILY(302, "临时重定向"),
    SEE_OTHER(303, "见其他重定向"),
    NOT_MODIFIED(304, "未修改"),
    USE_PROXY(305, "使用代理服务器"),
    TEMPORARY_REDIRECT(307, "请求的资源临时从不同的URI 响应请求"),

    //FAILURE CODES
    BAD_REQUEST(400, "错误的请求"),
    UNAUTHORIZED(401, "访问被拒绝"),
    PAYMENT_REQUIRED(402, "需要付款"),
    FORBIDDEN(403, "禁止访问"),
    NOT_FOUND(404, "所请求的页面不存在或已被删除"),
    METHOD_NOT_ALLOWED(405, "方法不被允许"),
    NOT_ACCEPTABLE(406, "客户端浏览器不接受所请求页面的 MIME 类型。 "),
    PROXY_AUTHENTICATION_REQUIRED(407, "需要代理验证"),
    REQUEST_TIMEOUT(408, "请求超时"),
    CONFLICT(409, "请求冲突"),
    GONE(410, "请求不可用,见其他"),
    LENGTH_REQUIRED(411, "需要有效长度"),
    PRECONDITION_FAILED(412, "前提条件失败"),
    REQUEST_ENTITY_TOO_LARGE(413, "请求实体太大"),
    REQUEST_URI_TOO_LONG(414, "请求的URI长度超过了服务器能够解释的长度"),
    UNSUPPORTED_MEDIA_TYPE(415, "不支持的媒体类型"),
    LOCKED(423, "当前资源被锁定"),
    UNAVAILABLE_FOR_LEGAL_REASONS(451, "该请求因法律原因不可用"),

    //Server Error Codes
    INTERNAL_SERVER_ERROR(500, "服务器内部异常"),
    NOT_IMPLEMENTED(501, "未实现"),
    BAD_GATEWAY(502, "错误的网关"),
    SERVER_UNAVAILABLE(503, "服务器不可用"),
    GATEWAY_TIMEOUT(504, "网关超时"),
    HTTP_VERSION_NOT_SUPPORTED(505, "HTTP版本不受支持"),

    //自定义异常
    FALLBACK(-1, "远程调用失败"),
    UNKNOWN_ERROR(1, "未知错误"),
    SYSTEM_BUSY(2, "系统繁忙，请稍候再试"),
    PARAMS_ERROR(3, "参数错误"),

    //
    ;

    private Integer code;
    private String desc;

    @Override
    public ErrorCodeEnum getDefault() {
        return null;
    }
}
