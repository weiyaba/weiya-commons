package com.weiya.commons.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.weiya.commons.enums.EnumInterface;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 分页返回
 *
 * @author dufu
 * @date 2018/3/27 下午8:17
 * @Copyright 北京伊电园网络科技有限公司 2016-2017 © 版权所有 京ICP备17000101号
 */
@Data
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of", access = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageResult<T> implements Serializable {

    private static final long serialVersionUID = -4257092979237049883L;

    private boolean success = false;
    private Integer code;
    private String msg;
    /**
     * 列表数据
     */
    private List<T> data;
    private Integer totalCount;
    private Integer pageNo;
    private Integer pageSize;
    /**
     * 列表扩展信息
     */
    private Map<String, Object> extra;

    /**
     * 正常返回
     *
     * @param data 数据内容
     * @param <T> 返回数据的类型
     * @return 包装后的返回
     */
    public static <T> PageResult<T> create(List<T> data) {
        return PageResult.<T>of().setSuccess(true).setData(data);
    }

    public static <T> PageResult<T> create(List<T> data, int totalCount, int pageNo, int pageSize) {
        return create(data).setTotalCount(totalCount).setPageNo(pageNo).setPageSize(pageSize);
    }

    public static <T> PageResult<T> create(List<T> data, Map extra, int totalCount, int pageNo, int pageSize) {
        return create(data, totalCount, pageNo, pageSize).setExtra(extra);
    }

    /**
     * 错误返回
     */
    public static <T> PageResult<T> error(EnumInterface errorCode) {
        return PageResult.<T>of().setSuccess(false).setCode(errorCode.getCode()).setMsg(errorCode.getDesc());
    }

    /**
     * 错误返回
     */
    public static <T> PageResult<T> error(EnumInterface errorCodeEnum, String msg) {
        return PageResult.<T>error(errorCodeEnum).setMsg(msg);
    }

}