package com.weiya.commons.entity;

import java.util.Date;
import lombok.Data;

@Data
public class BaseEntity {

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * @see com.weiya.commons.enums.DeletedEnum
     */
    private Integer deleted;
}