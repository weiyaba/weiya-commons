package com.weiya.commons.enums;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 逻辑删除字段 <br/>
 *
 * @author dufu
 * @date 2018/9/6 下午7:51
 * @Copyright 北京伊电园网络科技有限公司 2016-2017 © 版权所有 京ICP备17000101号
 */
@Getter
@AllArgsConstructor
public enum DeletedEnum implements EnumInterface<DeletedEnum> {

    DELETED(1, "删除"),
    NOT_DELETED(0, "未删除");

    private Integer code;
    private String desc;

    /**
     * 是否已删除
     */
    public static boolean isDeleted(Integer code) {
        return !Objects.equals(DeletedEnum.NOT_DELETED.getCode(), code);
    }

    public static DeletedEnum from(boolean deleted) {
        return deleted ? DeletedEnum.DELETED : DeletedEnum.NOT_DELETED;
    }

    /**
     * 默认为未删除
     *
     * @return 默认枚举对象
     */
    @Override
    public DeletedEnum getDefault() {
        return NOT_DELETED;
    }
}
