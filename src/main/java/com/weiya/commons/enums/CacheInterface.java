package com.weiya.commons.enums;

import java.util.concurrent.TimeUnit;

/**
 * 缓存定义接口 <br/>
 *
 * @author dufu
 * @date 2018/10/11 下午3:33
 * @Copyright 北京伊电园网络科技有限公司 2016-2017 © 版权所有 京ICP备17000101号
 */
public interface CacheInterface<T extends CacheInterface> {

    /**
     * 缓存key
     */
    String getKey();

    /**
     * 缓存key
     *
     * @param namespace 命名空间
     */
    String getKey(String namespace);

    /**
     * 描述
     */
    String getDesc();

    /**
     * 过期时间
     */
    Long getExpire();

    /**
     * 时间单位
     */
    TimeUnit getUnit();


}
