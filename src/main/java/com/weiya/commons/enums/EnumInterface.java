package com.weiya.commons.enums;

/**
 * @author 奕超
 */
public interface EnumInterface<T extends EnumInterface> {

    /**
     * 获取描述
     */
    String getDesc();

    /**
     * 获取code
     */
    Integer getCode();

    /**
     * 获取枚举name
     */
    String name();

    /**
     * 获取枚举默认值
     */
    T getDefault();
}
