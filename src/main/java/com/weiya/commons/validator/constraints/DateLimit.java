package com.weiya.commons.validator.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.weiya.commons.validator.constraintvalidators.DateLimitValidatorImpl;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = DateLimitValidatorImpl.class)
public @interface DateLimit {

    String message() default "时间必须在{min}和{max}之间";

    /**
     * 最小偏移量
     */
    int min() default 0 ;

    /**
     * 最大偏移量
     */
    int max() default 0;

    /**
     * 时间单位：{SECONDS,MINUTES,HOURS,DAYS}
     */
    TimeUnit unit() default TimeUnit.DAYS;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Defines several {@link DateLimit} annotations on the same element.
     *
     * @see DateLimit
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    @interface List {

        DateLimit[] value();
    }
}