package com.weiya.commons.validator.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.weiya.commons.validator.constraintvalidators.RegexMatchValidatorImpl;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * StartsWith
 *
 * @author qingzhi
 * @desc 输入字符串是否满足正则匹配
 * @see javax.validation.constraints.Pattern
 * @date 18/5/3
 */
@Target({ANNOTATION_TYPE, METHOD, FIELD, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = RegexMatchValidatorImpl.class)//此处指定了注解的实现类为RegexMatchValidatorImpl
public @interface RegexMatch {

    /**
     * 添加正则表达式属性，作为校验条件
     */
    String regex() default "[0-9]*";

    String message() default "输入字符串格式不正确";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    /**
     * Defines several {@code @Length} annotations on the same element.
     */
    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        RegexMatch[] value();
    }
}