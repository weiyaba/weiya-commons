package com.weiya.commons.codec;

import com.weiya.commons.util.LocalMapUtils;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Assert;
import org.junit.Test;

/**
 * LocalMapUtilsTest
 *
 * @author qingzhi
 * @desc list与map互转常用工具类单元测试
 * @date 18/8/9
 */
public class LocalMapUtilsTest {

    private static List<Student> students = Stream.of(
            Student.of(1, "刘一", 60),
            Student.of(2, "陈二", 59),
            Student.of(3, "张三", 91),
            Student.of(4, "李四", 100),
            Student.of(5, "王五", 86),
            Student.of(6, "赵六", 86),
            Student.of(7, "孙七", 98),
            Student.of(8, "周八", 78),
            Student.of(9, "吴九", 91),
            Student.of(10, "郑十", 86)
    ).collect(Collectors.toList());

    /**
     * @see LocalMapUtils#listAsHashMap(List, java.util.function.Function)
     */
    @Test
    public void testListAsHashMap1() {
        Map<Integer, Student> resultMap = LocalMapUtils.listAsHashMap(students, Student::getId);
        System.out.println(resultMap);
        Assert.assertEquals("list大小与map大小一致", students.size(), resultMap.size());
        Stream.iterate(1, i -> i + 1).limit(10).forEach(i -> Assert
                .assertEquals("list第" + i + "个元素与map的key=" + i + "一致", students.get(i - 1), resultMap.get(i)));
    }

    /**
     * @see LocalMapUtils#listAsHashMap(List, java.util.function.Function, java.util.function.Function)
     */
    @Test
    public void testListAsHashMap2() {
        Map<Integer, SubStudent> resultMap = LocalMapUtils.listAsHashMap(students, Student::getId, SubStudent::of);
        System.out.println(resultMap);
        Assert.assertEquals("list大小与map大小一致", students.size(), resultMap.size());
        Stream.iterate(1, i -> i + 1).limit(10).forEach(i -> Assert
                .assertEquals("list第" + i + "个元素与map的key=" + i + "一致", SubStudent.of(students.get(i - 1)),
                        resultMap.get(i)));
    }

    @Test
    public void testListAsHashMapList() {
        Map<Integer, List<Student>> resultMap = LocalMapUtils.listAsHashMapList(students, Student::getScore);
        System.out.println(resultMap);
        Assert.assertTrue(
                students.size() == resultMap.values().stream().collect(Collectors.summarizingInt(List::size)).getSum());
        Assert.assertTrue(resultMap.get(59).size() == 1);
        Assert.assertTrue(resultMap.get(60).size() == 1);
        Assert.assertTrue(resultMap.get(91).size() == 2);
        Assert.assertTrue(resultMap.get(100).size() == 1);
        Assert.assertTrue(resultMap.get(86).size() == 3);
        Assert.assertTrue(resultMap.get(98).size() == 1);
        Assert.assertTrue(resultMap.get(78).size() == 1);
    }

    /**
     * 遇到重复key，后者覆盖前者
     *
     * @see LocalMapUtils#collectionToMap(java.util.Collection, java.util.function.Function)
     */
    @Test
    public void testListToMapCover1() {
        Map<Integer, Student> resultMap = LocalMapUtils.collectionToMap(students, Student::getScore);
        System.out.println(resultMap);
        Assert.assertEquals("list大小与map大小一致", 7, resultMap.size());
        Collections.reverse(students);
        Stream.of(59, 60, 91, 100, 86, 98, 78)
                .forEach(sco -> Assert.assertEquals("list分数为" + sco + "最后一个的元素与map的key=" + sco + "一致",
                        students.stream().filter(stu -> stu.getScore().equals(sco)).findFirst().get(),
                        resultMap.get(sco)));
    }

    /**
     * 遇到重复key，后者覆盖前者
     *
     * @see LocalMapUtils#collectionToMap(java.util.Collection, java.util.function.Function, java.util.function.Function)
     */
    @Test
    public void testListAsMapCover2() {
        Map<Integer, SubStudent> resultMap = LocalMapUtils.collectionToMap(students, Student::getScore, SubStudent::of);
        System.out.println(resultMap);
        Assert.assertEquals("list大小与map大小一致", 7, resultMap.size());
        Collections.reverse(students);
        Stream.of(59, 60, 91, 100, 86, 98, 78)
                .forEach(sco -> Assert.assertEquals("list分数为" + sco + "最后一个的元素与map的key=" + sco + "一致",
                        SubStudent.of(students.stream().filter(stu -> stu.getScore().equals(sco)).findFirst().get()),
                        resultMap.get(sco)));
    }

    /**
     * 遇到重复key，保留原者
     *
     * @see LocalMapUtils#collectionToMap(java.util.Collection, java.util.function.Function, Boolean)
     */
    @Test
    public void testListToMap1() {
        Map<Integer, Student> resultMap = LocalMapUtils.collectionToMap(students, Student::getScore, false);
        System.out.println(resultMap);
        Assert.assertEquals("list大小与map大小一致", 7, resultMap.size());
        Stream.of(59, 60, 91, 100, 86, 98, 78)
                .forEach(sco -> Assert.assertEquals("list分数为" + sco + "最后一个的元素与map的key=" + sco + "一致",
                        students.stream().filter(stu -> stu.getScore().equals(sco)).findFirst().get(),
                        resultMap.get(sco)));
    }

    /**
     * 遇到重复key，保留原者
     *
     * @see LocalMapUtils#collectionToMap(java.util.Collection, java.util.function.Function, java.util.function.Function, Boolean)
     */
    @Test
    public void testListAsMap2() {
        Map<Integer, SubStudent> resultMap = LocalMapUtils
                .collectionToMap(students, Student::getScore, SubStudent::of, false);
        System.out.println(resultMap);
        Assert.assertEquals("list大小与map大小一致", 7, resultMap.size());
        Stream.of(59, 60, 91, 100, 86, 98, 78)
                .forEach(sco -> Assert.assertEquals("list分数为" + sco + "最后一个的元素与map的key=" + sco + "一致",
                        SubStudent.of(students.stream().filter(stu -> stu.getScore().equals(sco)).findFirst().get()),
                        resultMap.get(sco)));
    }

    /**
     * 分组
     *
     * @see LocalMapUtils#collectionGroupBy(java.util.Collection, java.util.function.Function)
     */
    @Test
    public void testListGroupBy() {
        Map<Integer, List<Student>> resultMap = LocalMapUtils.collectionGroupBy(students, Student::getScore);
        System.out.println(resultMap);
        Assert.assertTrue(
                students.size() == resultMap.values().stream().collect(Collectors.summarizingInt(List::size)).getSum());
        Assert.assertTrue(resultMap.get(59).size() == 1);
        Assert.assertTrue(resultMap.get(60).size() == 1);
        Assert.assertTrue(resultMap.get(91).size() == 2);
        Assert.assertTrue(resultMap.get(100).size() == 1);
        Assert.assertTrue(resultMap.get(86).size() == 3);
        Assert.assertTrue(resultMap.get(98).size() == 1);
        Assert.assertTrue(resultMap.get(78).size() == 1);
    }

    /**
     * 分组并自定义收集器
     *
     * @see LocalMapUtils#collectionGroupBy(java.util.Collection, java.util.function.Function, java.util.stream.Collector)
     */
    @Test
    public void testListGroupByAndCollect() {
        Map<Integer, List<SubStudent>> resultMap = LocalMapUtils.collectionGroupBy(students, Student::getScore,
                Collectors.mapping(SubStudent::of, Collectors.toList()));
        System.out.println(resultMap);
        Assert.assertTrue(
                students.size() == resultMap.values().stream().collect(Collectors.summarizingInt(List::size)).getSum());
        Assert.assertTrue(resultMap.get(59).size() == 1);
        Assert.assertTrue(resultMap.get(60).size() == 1);
        Assert.assertTrue(resultMap.get(91).size() == 2);
        Assert.assertTrue(resultMap.get(100).size() == 1);
        Assert.assertTrue(resultMap.get(86).size() == 3);
        Assert.assertTrue(resultMap.get(98).size() == 1);
        Assert.assertTrue(resultMap.get(78).size() == 1);
        Assert.assertTrue(resultMap.get(59).get(0).getLevel().equals("不及格"));
        Assert.assertTrue(resultMap.get(60).get(0).getLevel().equals("差"));
        Assert.assertTrue(resultMap.get(91).get(0).getLevel().equals("优"));
        Assert.assertTrue(resultMap.get(100).get(0).getLevel().equals("💯"));
        Assert.assertTrue(resultMap.get(86).get(0).getLevel().equals("良"));
        Assert.assertTrue(resultMap.get(98).get(0).getLevel().equals("优"));
        Assert.assertTrue(resultMap.get(78).get(0).getLevel().equals("中"));
    }


    /**
     * 分组
     *
     * @see LocalMapUtils#collectionPartitionBy(java.util.Collection, java.util.function.Predicate)
     */
    @Test
    public void testListPartitionBy() {
        Map<Boolean, List<Student>> resultMap = LocalMapUtils
                .collectionPartitionBy(students, stu -> stu.getScore() > 80);
        System.out.println(resultMap);
        Assert.assertTrue(
                students.size() == resultMap.values().stream().collect(Collectors.summarizingInt(List::size)).getSum());
        Assert.assertTrue(resultMap.get(true).size() == 7);
        Assert.assertTrue(resultMap.get(false).size() == 3);
    }

    /**
     * 分组并自定义收集器
     *
     * @see LocalMapUtils#collectionPartitionBy(java.util.Collection, java.util.function.Predicate, java.util.stream.Collector)
     */
    @Test
    public void testListPartitionByAndCollect() {
        Map<Boolean, List<SubStudent>> resultMap = LocalMapUtils
                .collectionPartitionBy(students, stu -> stu.getScore() > 80,
                        Collectors.mapping(SubStudent::of, Collectors.toList()));
        System.out.println(resultMap);
        Assert.assertTrue(
                students.size() == resultMap.values().stream().collect(Collectors.summarizingInt(List::size)).getSum());
        Assert.assertTrue(resultMap.get(true).size() == 7);
        Assert.assertTrue(resultMap.get(false).size() == 3);
        Assert.assertTrue(resultMap.get(true).get(0).getLevel().equals("优"));
        Assert.assertTrue(resultMap.get(false).get(0).getLevel().equals("差"));
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor(staticName = "of")
    static class Student {

        private Integer id;
        private String name;
        private Integer score;

        @Override
        public String toString() {
            return "{" +
                    "\"id\":" + id +
                    ", \"name\":\"" + name + '\"' +
                    ", \"score\":" + score +
                    '}';
        }
    }

    @Data
    @NoArgsConstructor
    static class SubStudent extends Student {

        private String level;

        public static SubStudent of(Student student) {
            SubStudent subStudent = new SubStudent();
            subStudent.setId(student.getId());
            subStudent.setName(student.getName());
            subStudent.setScore(student.getScore());
            if (student.getScore() < 60) {
                subStudent.level = "不及格";
            } else if (student.getScore() < 70) {
                subStudent.level = "差";
            } else if (student.getScore() < 80) {
                subStudent.level = "中";
            } else if (student.getScore() < 90) {
                subStudent.level = "良";
            } else if (student.getScore() < 100) {
                subStudent.level = "优";
            } else {
                subStudent.level = "💯";
            }
            return subStudent;
        }

        @Override
        public String toString() {
            return "{" +
                    "\"id\":" + super.id +
                    ", \"name\":\"" + super.name + '\"' +
                    ", \"level\":\"" + level + '\"' +
                    ", \"score\":" + super.score +
                    '}';
        }
    }
}
