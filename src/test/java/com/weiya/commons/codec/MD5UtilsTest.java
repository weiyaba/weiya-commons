package com.weiya.commons.codec;

import org.junit.Assert;
import org.junit.Test;

public class MD5UtilsTest {

    @Test
    public void encrypt() {
        String data = MD5Utils.encrypt("123456");
        System.out.println(data);
        Assert.assertEquals("MD5加密不一致", "e10adc3949ba59abbe56e057f20f883e", data);
    }

    @Test
    public void encryptWithSalt() {
        String data = MD5Utils.encrypt("123456", "md5");
        System.out.println(data);
        Assert.assertEquals("MD5加密不一致", "ae8176b4caaf5f39283361ae3eacc71f", data);
    }
}