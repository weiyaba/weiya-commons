package com.weiya.commons.codec;

import org.junit.Assert;
import org.junit.Test;

public class Base64UtilsTest {

    @Test
    public void encrypt() {
        String data = Base64Utils.encrypt("123456");
        System.out.println(data);
        Assert.assertEquals("BASE64加密不一致", "MTIzNDU2", data);
    }

    @Test
    public void decrypt() {
        String data = Base64Utils.decrypt("MTIzNDU2");
        System.out.println(data);
        Assert.assertEquals("BASE64加密不一致", "123456", data);
    }
}