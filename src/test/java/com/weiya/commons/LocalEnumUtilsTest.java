package com.weiya.commons;

import cn.hutool.core.collection.CollectionUtil;
import com.weiya.commons.enums.DeletedEnum;
import com.weiya.commons.util.LocalEnumUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.junit.Assert;
import org.junit.Test;

/**
 * LocalEnumUtilsTest
 *
 * @author 奕超
 * @date 2018/9/11
 */
public class LocalEnumUtilsTest {
    @Test
    public void testContainAllCode() {
        Assert.assertTrue(!LocalEnumUtils.containAllCode(DeletedEnum.class, null));
        Assert.assertTrue(!LocalEnumUtils.containAllCode(DeletedEnum.class, new ArrayList<>()));
        List<Integer> codeList = new ArrayList<>();
        codeList.add(0);
        Assert.assertTrue(LocalEnumUtils.containAllCode(DeletedEnum.class, codeList));
        codeList.add(1);
        Assert.assertTrue(LocalEnumUtils.containAllCode(DeletedEnum.class, codeList));
        codeList.add(2);
        Assert.assertTrue(!LocalEnumUtils.containAllCode(DeletedEnum.class, codeList));
    }

    @Test
    public void testFindByCodeList() {
        List<DeletedEnum> result = LocalEnumUtils.findByCodeList(DeletedEnum.class, null);
        Assert.assertTrue(CollectionUtil.isEmpty(result));
        result = LocalEnumUtils.findByCodeList(DeletedEnum.class, new ArrayList<>());
        Assert.assertTrue(CollectionUtil.isEmpty(result));
        List<Integer> codeList = new ArrayList<>();
        codeList.add(2);
        result = LocalEnumUtils.findByCodeList(DeletedEnum.class, codeList);
        Assert.assertTrue(CollectionUtil.isEmpty(result));
        codeList.add(0);
        result = LocalEnumUtils.findByCodeList(DeletedEnum.class, codeList);
        Assert.assertTrue(Objects.equals(DeletedEnum.NOT_DELETED, result.get(0)));
        codeList.add(1);
        result = LocalEnumUtils.findByCodeList(DeletedEnum.class, codeList);
        Assert.assertTrue(Objects.equals(2, result.size()));
    }

}
