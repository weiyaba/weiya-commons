package com.weiya.commons.hutools;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import java.util.Arrays;
import org.junit.Test;

public class HutoolTest {

    @Test
    public void writer() {
        int num = RandomUtil.randomInt();
        System.out.println(num);
        ExcelWriter writer = ExcelUtil.getWriter("a.xlsx");
        writer.write(Arrays.asList(num));
        writer.close();
    }

    @Test
    public void reader() {
        ExcelReader reader = ExcelUtil.getReader("a.xlsx");
        reader.read().forEach(map -> {
            System.out.println(map);
        });
        reader.close();
    }


}