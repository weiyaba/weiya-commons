package com.weiya.commons.constraintvalidators;

import cn.hutool.core.date.DateUtil;
import com.weiya.commons.validator.constraintvalidators.DateLimitValidatorImpl;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.Test;

public class DateLimitValidatorImplTest {

    private int min;

    private int max;

    private TimeUnit unit;


    @Test
    public void isValid() {
        Date now = new Date();
        DateLimitValidatorImpl validator = new DateLimitValidatorImpl();

        //输入的时间在预期的范围内
        validator.setMin(0);
        validator.setMax(0);
        validator.setUnit(TimeUnit.DAYS);
        boolean flag1 = validator.isValid(null, null);
        Assert.assertTrue("输入的时间在偏移时间之后", flag1);

        validator.setMin(0);
        validator.setMax(7);
        validator.setUnit(TimeUnit.SECONDS);
        Date date2 = DateUtil.offsetDay(now, 5);
        boolean flag2 = validator.isValid(date2, null);
        Assert.assertTrue("输入的时间在偏移时间之前", flag2);

        validator.setMin(7);
        validator.setMax(0);
        validator.setUnit(TimeUnit.MINUTES);
        Date date3 = DateUtil.offsetDay(now, -5);
        boolean flag3 = validator.isValid(date3, null);
        Assert.assertTrue("输入的时间在偏移时间之前", flag3);

        validator.setMin(7);
        validator.setMax(7);
        validator.setUnit(TimeUnit.DAYS);
        Date date4 = DateUtil.offsetDay(now, 5);
        boolean flag4 = validator.isValid(date4, null);
        Assert.assertTrue("输入的时间在偏移时间之前", flag4);

    }
}